// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
/**
 * Version upgrader:
 * ------------
 */
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
import groovy.json.JsonSlurper
import groovy.xml.StreamingMarkupBuilder
import groovy.util.XmlNodePrinter
import org.codehaus.groovy.tools.xml.DomToGroovy
import groovy.xml.XmlUtil

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
def projectListMap = [: ]
def rootProjectId = 7759130
def PRIVATE_TOKEN = build.getEnvironment(listener).get('PRIVATE_TOKEN')
def groupId = build.getEnvironment(listener).get('groupId')
def artifactId = build.getEnvironment(listener).get('artifactId')
def newVersion = build.getEnvironment(listener).get('newVersion')

// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

  try {
    // Retrieve the various group in the EJ4Tezos group:
    def get = new URL("https://gitlab.com/api/v4/groups?id=" + rootProjectId).openConnection();
    def getRC = get.getResponseCode();

    if (getRC.equals(200)) {

      def jsonSlurper = new JsonSlurper()
      def projectList = jsonSlurper.parseText(get.getInputStream().getText())

      projectList.each {
        projectListMap.putAt(it.name, it.id)
      }

    }
    else {
      println("Error while accessing the group details! code=" + getRC);
      return;
    }
    projectListMap.each {
      entry ->

      def urlGroup = "https://gitlab.com/api/v4/groups/" + entry.value + "/projects"

      def getGroup = new URL(urlGroup).openConnection();
      def getGroupRC = getGroup.getResponseCode();

      if (getGroupRC.equals(200)) {

        def jsonSlurper = new JsonSlurper()
        def projectList = jsonSlurper.parseText(getGroup.getInputStream().getText())

        projectList.each {

          def projectId = it.id
          def projectName = it.path
          def repo = it.http_url_to_repo

          println("Working on: " + projectName);


          def urlPom = "https://gitlab.com/api/v4/projects/${projectId}/repository/files/pom.xml/raw?ref=develop"
          def getPom = new URL(urlPom).openConnection();
          def getPomRC = getPom.getResponseCode()


          //check if the it's a maven prject
          if(getPomRC.equals(200)) {
            def pom = new XmlSlurper(false, false).parseText(getPom.getInputStream().getText())

            def changed = false

            // check if it's in the same group
            if (pom.groupId.equals(groupId)) {
              //check if it's the parent
              if (pom.parent.artifactId.equals(artifactId)) {
                pom.parent.version = newVersion
                changed = true
              }

              // if not in parent check in dependencies
              if (!changed) {
                pom.dependencies.dependency.each {

                  if (it.artifactId.equals(artifactId)) {
                    it.version = newVersion
                    changed = true
                  }
                }
              }

              // if not in dependencies check in plugins
              if (!changed) {
                pom.build.plugins.plugin.each {
                  if (it.artifactId.equals(artifactId)) {
                    it.version = newVersion
                    changed = true

                  }
                }

              }

              if (changed) {


                // output the pom
                println "Writing new pom.xml for ${projectName}"
                def outputBuilder = new StreamingMarkupBuilder()
                String result = outputBuilder.bind {
                  mkp.yield pom
                }


                def newPomContent =  XmlUtil.serialize(result).toString()
                println newPomContent

                // Commit the changes
                def commitURL = "https://gitlab.com/api/v4/projects/${projectId}/repository/commits"

                def commitPost = new URL(commitURL).openConnection()
                def body = """
                {
                "branch": "develop",
                "commit_message": "Jenkins update version script: change $artifactId version to  $newVersion",
                "actions": [
                {
                  "action": "update",
                  "file_path": "pom.xml",
                  "content": "${newPomContent.bytes.encodeBase64().toString()}",
                  "encoding": "base64"
                  }
                ]
                }
                """
                commitPost.setDoOutput(true)
                commitPost.setRequestProperty("Content-Type", "application/json")
                commitPost.setRequestProperty("PRIVATE-TOKEN", PRIVATE_TOKEN)
                commitPost.getOutputStream().write(body.getBytes("UTF-8"));
                def commitPostRC = commitPost.getResponseCode();
                println(commitPostRC);
                if(commitPostRC.equals(200)) {
                    println(commitPost.getInputStream().getText());
                }
                println "OK"
              }

            }
          }
        }

      }
      else {
        return;
      }

    }
  } catch(err) {
    throw err
  }